import create from 'zustand';

export interface UseStorage {
  isLoading: boolean;
  error: string | any;
  setIsLoading: (state: boolean) => void;
  setError: (error: any) => void;
}

const useStorage = create<UseStorage>((set) => ({
  isLoading: false,
  error: null,
  setIsLoading: (state: boolean) => set(() => ({ isLoading: state })),
  setError: (error: any) => {
    set(() => ({ error }));
  },
}));

export default useStorage;
