import * as React from 'react';

export type Envionments = 'development' | 'production';

export type Variants = 'primary' | 'secondary' | 'outlined';

export interface Fields {
  documentType: string | number;
  documentId: string;
  fees: string | number;
}

export interface CredinetConfig {
  vendorId: string;
  apiKey: string;
  env?: Envionments;
}

export interface PaymentButtonProps {
  callback: string;
  subtotal: number;
  defaultValues?: Fields;
  credinetConfig: CredinetConfig;
}

export interface PaymentModalProps {
  defaultValues?: Fields | undefined;
  loading?: boolean;
  onClose: () => void;
  onPaymentClick: () => void;
}

export interface TextFieldProps extends React.HTMLProps<HTMLInputElement> {
  id: string;
  label: string;
  error?: boolean;
  helperText?: string | undefined;
}

export interface SelectProps extends React.HTMLProps<HTMLSelectElement> {
  id: string;
  label: string;
  error?: boolean;
  helperText?: string | undefined;
  children?: JSX.Element[];
}

export interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  variant?: Variants;
  type: 'button' | 'submit' | 'reset';
}

export interface GetLimitInMonthsParams {
  creditValue: string;
  storeId: string;
  typeDocument: string;
  idDocument: string;
}

export interface GetLimitInMonthsResponse {
  months: number;
}

export interface GetCreditDetailsParams {
  CreditValue: string;
  Months: number;
  Frequency: number;
  TypeDocument: string;
  IdDocument: string;
}

export interface ServiceResponse<T> {
  errorCode: number;
  message: string;
  country: string;
  data: T;
}

export interface GetCreditDetailsResponse {
  downPayment?: number;
  totalFeeValue?: number;
  creditValue?: number;
  fees?: number;
  assuranceValue?: number;
  interestRate?: number;
  totalInterestValue?: number;
  totalDownPayment?: number;
  feeCreditValue?: number;
  assuranceFeeValue?: number;
  assuranceTotalValue?: number;
  assuranceTaxFeeValue?: number;
  assuranceTaxValue?: number;
  downPaymentPercentage?: number;
  assurancePercentage?: number;
  assuranceTotalFeeValue?: number;
  totalPaymentValue?: number;
}

export interface GetCreditTokenParams {
  CreditValue: string;
  Months: number | string;
  Frequency: number;
  TypeDocument: string;
  IdDocument: string;
}

export interface GetCreditTokenResponse {
  token: {
    value: string;
    remainingSeconds: number;
  };
  formattedToken: string;
  totalTime: number;
  duration: string;
  expirationDate: string;
  generationDate: string;
}

export interface CreateCerditData {
  typeDocument: string;
  idDocument: string;
  creditValue: number;
  frequency: number;
  fees: number;
  source: number;
  token: string;
  storeId: string;
  userId: string;
  seller: string;
  products: string;
  invoice: string;
  userName: string;
}

export interface CreateCreditResponse {
  typeDocument: string;
  idDocument: string;
  creditId: string;
  creditNumber: number;
  effectiveAnnualRate: number;
  downPayment: number;
  totalFeeValue: number;
  creditValue: number;
  fees: number;
  assuranceValue: number;
  interestRate: number;
  totalInterestValue: number;
  totalDownPayment: number;
  feeCreditValue: number;
  assuranceFeeValue: number;
  assuranceTotalValue: number;
  assuranceTaxFeeValue: number;
  assuranceTaxValue: number;
  downPaymentPercentage: number;
  assurancePercentage: number;
  assuranceTotalFeeValue: number;
  downPaymentId: string;
}

export interface CheckoutProps {
  onClose?: () => void;
  creditValue: number;
  seller: string;
  invoice: string;
  products: string;
  responseUrl: string;
}
