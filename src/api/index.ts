import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import {
  ServiceResponse,
  GetCreditDetailsParams,
  GetCreditDetailsResponse,
  GetCreditTokenParams,
  GetCreditTokenResponse,
  CreateCerditData,
  CreateCreditResponse,
} from '../types';

import useStorage from '../storage/app.storage';

const Axios: AxiosInstance = axios.create({
  baseURL: process.env.API_BASE,
  headers: {
    SCOrigen: 'Development',
    SCLocation: '1.1',
    country: 'co',
  },
});

export interface Error {
  codigo: string;
  mensaje: string;
}

export interface ServiceErrorResponse {
  errores: Error[];
}

const useRequest = () => {
  const { setIsLoading, setError } = useStorage();

  Axios.interceptors.response.use(
    (response: AxiosResponse): AxiosResponse => response,
    (error: AxiosError<ServiceErrorResponse>): void => {
      setError(error.response?.data.errores.map((err) => err.mensaje.split('\r\n')[0]).join('\n'));
    }
  );

  async function Request<T>(config: AxiosRequestConfig): Promise<T> {
    try {
      setIsLoading(true);
      const response = await Axios(config);
      return response?.data;
    } finally {
      setIsLoading(false);
    }
  }

  const getCreditDetails = async (params: GetCreditDetailsParams): Promise<GetCreditDetailsResponse> => {
    const response = await Request<ServiceResponse<GetCreditDetailsResponse>>({
      method: 'GET',
      url: '/Credits/GetCreditDetails',
      params,
    });
    return response?.data;
  };

  const getCreditToken = async (params: GetCreditTokenParams): Promise<GetCreditTokenResponse> => {
    const response = await Request<ServiceResponse<GetCreditTokenResponse>>({
      method: 'GET',
      url: '/Credits/GetCreditToken',
      params,
    });
    return response?.data;
  };

  const createCredit = async (body: CreateCerditData): Promise<CreateCreditResponse> => {
    const response = await Request<ServiceResponse<CreateCreditResponse>>({
      method: 'POST',
      url: '/credits/create',
      data: body,
    });
    return response?.data;
  };

  return {
    getCreditDetails,
    getCreditToken,
    createCredit,
  };
};

export default useRequest;
