// /* eslint-disable no-unused-vars */
// import * as React from 'react';
import * as React from 'react';
// import { Checkout } from './Checkout';
import { CheckoutProps } from '../types';

// export const ButtonCheckout: React.FC<ButtonCheckoutProps> = ({ renderInput, ...props }): JSX.Element => {
//   const [open, setOpen] = React.useState(false);
//   return (
//     <div>
//       {renderInput({ onClick: () => setOpen(true) })}
//       {open && <Checkout onClose={() => setOpen(false)} {...props} />}
//     </div>
//   );
// };

export interface RenderInputParams {
  onClick: () => void;
}

export interface ButtonCheckoutProps extends CheckoutProps {
  renderInput: React.ReactNode;
  open: boolean;
}

export const ButtonCheckout: React.FC<ButtonCheckoutProps> = ({ renderInput, open }) => {
  const [hell] = React.useState('hello');
  return (
    <>
      {renderInput}
      {/* {open && <Checkout {...props} />} */}
      {open && <div>{hell}</div>}
    </>
  );
};
