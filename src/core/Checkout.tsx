/* eslint-disable no-restricted-globals */
/* eslint-disable no-console */
/** MISC Imports */
import * as React from 'react';
import { useForm } from 'react-hook-form';

/** Utils Imports */
import currency from '../utils/currencyFormat';

/** API */
import useApi from '../api';

/** Storage */
import useStorage from '../storage/app.storage';

/** Types */
import { CreateCerditData, GetCreditDetailsResponse, CheckoutProps } from '../types';

/** Custom Components */
import { Modal, Select, TextField, Button, Banner, Loading } from '../components';

export function Checkout({ onClose, creditValue, seller, products, invoice, responseUrl }: CheckoutProps) {
  /** Component States */
  const [documentTypes] = React.useState([
    {
      name: 'CC',
      description: 'Cédula de Ciudadanía',
    },
    {
      name: 'CE',
      description: 'Cédula de Extranjería',
    },
  ]);
  const [months] = React.useState<number[]>([1, 2, 3, 4, 5, 6]);
  const [creditDetails, setCreditDetails] = React.useState<GetCreditDetailsResponse>();
  const [counter, setCounter] = React.useState<number>(0);

  /** Storage */
  const { isLoading, error } = useStorage();

  /** Custom Hooks */
  const { getCreditDetails, getCreditToken, createCredit } = useApi();
  const {
    handleSubmit,
    register,
    getValues,
    setValue,
    formState: { errors, touchedFields },
  } = useForm<CreateCerditData>({
    defaultValues: {
      userName: 'Prueba',
      frequency: 30,
      source: 4,
      creditValue,
      products,
      invoice,
      seller,
    },
  });

  React.useEffect(() => {
    if (counter && counter > 0) {
      setTimeout(() => {
        setCounter(counter - 1);
      }, 1000);
    }
  }, [counter]);

  const getDetails = async (e: React.FormEvent<HTMLSelectElement>) => {
    const params = {
      CreditValue: String(getValues('creditValue')),
      TypeDocument: String(getValues('typeDocument')),
      IdDocument: getValues('idDocument'),
      Months: +e.currentTarget.value,
      Frequency: getValues('frequency'),
    };
    const serviceCreditDetails = await getCreditDetails(params);
    if (serviceCreditDetails) {
      const serviceCreditToken = await getCreditToken(params);
      setCreditDetails(serviceCreditDetails);
      setCounter(serviceCreditToken.token.remainingSeconds);
      if (serviceCreditDetails.fees) {
        setValue('fees', +serviceCreditDetails.fees);
      }
    }
  };

  const getToken = async () => {
    const serviceCreditToken = await getCreditToken({
      CreditValue: String(getValues('creditValue')),
      TypeDocument: String(getValues('typeDocument')),
      IdDocument: getValues('idDocument'),
      Months: getValues('fees'),
      Frequency: getValues('frequency'),
    });
    setCounter(serviceCreditToken.token.remainingSeconds);
  };

  const onSubmit = async (data: CreateCerditData): Promise<void> => {
    const serviceCreateCredit = await createCredit(data);
    if (serviceCreateCredit && serviceCreateCredit.creditId) {
      const query = `?transactionState=4&creditId=${serviceCreateCredit.creditId}`;
      location.replace(responseUrl + query);
    }
  };

  return (
    <Modal>
      <div>
        <div className="pt-4 px-6">
          <img
            src="https://www.sistecredito.com/medios-alternos-de-pago/images/logo-sistecredito.png"
            alt="Logo Sistecrédito"
          />
          <div className="py-2">
            <h3 className="text-lg leading-6 font-medium text-gray-900">Paga con Sistecrédito</h3>
            <p className="mt-2 text-sm text-gray-500">
              Completa el siguiente formulario para comenzar con la solicitud de tu crédito.
            </p>
          </div>
        </div>
        {!!error && (
          <div className="p-1">
            <Banner variant="error">{error}</Banner>
          </div>
        )}
        <div className="">
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="px-6 py-2">
              <div className="py-2">
                <Select
                  id="typeDocument"
                  label="Tipo de documento"
                  placeholder="Selecciona el tipo de documento"
                  error={!!errors.typeDocument}
                  helperText={errors?.typeDocument?.message}
                  {...register('typeDocument')}
                >
                  {documentTypes.map(({ name, description }) => (
                    <option key={name} value={name}>
                      {description}
                    </option>
                  ))}
                </Select>
                <TextField
                  id="idDocument"
                  label="Número de documento"
                  placeholder="Ingrese su número de documento"
                  error={!!errors.idDocument}
                  helperText={errors?.idDocument?.message}
                  {...register('idDocument', {
                    required: {
                      value: true,
                      message: 'Este campo es requerido.',
                    },
                  })}
                />
                <Select
                  id="fees"
                  label="Cuotas"
                  placeholder="Seleccionar cuotas"
                  disabled={!touchedFields.typeDocument && !touchedFields.idDocument}
                  {...register('fees')}
                  onChange={(e) => getDetails(e)}
                >
                  {months.map((month) => (
                    <option key={month} value={month}>
                      {`${month} ${month === 1 ? 'cuota' : 'cuotas'}`}
                    </option>
                  ))}
                </Select>
              </div>
              {isLoading && (
                <div className="p-2">
                  <Loading label="Obteniendo información de tu crédito..." />
                </div>
              )}
              {creditDetails && (
                <div>
                  <div className="my-3 border border-gray-200" />
                  <div className="grid grid-cols-1 divide-y">
                    <div className="p-2 flex flex-row justify-between">
                      <span className="font-medium text-sm text-gray-400">Cuota</span>
                      <span className="">$ {creditDetails.totalFeeValue && currency(creditDetails.totalFeeValue)}</span>
                    </div>
                    <div className="p-2 flex flex-row justify-between font-medium text-lg">
                      <span>Total crédito</span>
                      <span>$ {creditDetails.totalPaymentValue && currency(creditDetails.totalPaymentValue)}</span>
                    </div>
                  </div>
                  <div className="py-2">
                    <TextField
                      id="token"
                      label="Token"
                      placeholder="Ingresa el token"
                      helperText={`Recibirás un token vía SMS o correo electrónico que expirará en ${counter} segundos.`}
                      type="number"
                      autoComplete="off"
                      {...register('token', {
                        required: {
                          value: true,
                          message: 'El token es requerido.',
                        },
                        minLength: {
                          value: 6,
                          message: 'El token debe contener mínimo 6 caracteres.',
                        },
                        maxLength: {
                          value: 6,
                          message: 'El token no debe contener más de 6 caracteres.',
                        },
                        pattern: {
                          value: /^[0-9]+$/,
                          message: 'Este campo sólo puede contener números.',
                        },
                      })}
                    />
                    {counter <= 0 && (
                      <Button type="button" onClick={getToken} disabled={counter !== 0}>
                        Reenviar Token
                      </Button>
                    )}
                  </div>
                </div>
              )}
            </div>
            <div className="flex flex-row-reverse p-6 bg-gray-100">
              <Button type="submit" variant="primary" disabled={!touchedFields.token}>
                Pagar
              </Button>
              <Button type="button" variant="outlined" onClick={onClose}>
                Cancelar
              </Button>
            </div>
          </form>
        </div>
      </div>
    </Modal>
  );
}
