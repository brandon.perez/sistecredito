export default function currencyFormat(value: string | number): string {
  return new Intl.NumberFormat('de-DE').format(+value).replace(/\D00$/, '');
}
