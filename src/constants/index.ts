import { Envionments } from '../types';

export const API_BASE = (env: Envionments = 'development'): string =>
  env === 'development' ? 'https://devapi.credinet.co/pos/' : 'https://api.credinet.co/pos/';
