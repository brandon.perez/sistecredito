import * as React from 'react';
import cx from 'clsx';
import { TextFieldProps } from '../types';

export const TextField = React.forwardRef<HTMLInputElement, TextFieldProps>(
  ({ id, label, helperText, error, ...props }, ref) => {
    return (
      <div className="py-2">
        <label className={cx('block text-sm font-medium', error ? 'text-red-500' : 'text-gray-700')} htmlFor={id}>
          {label}
        </label>
        <input
          className={cx(
            'block w-full mt-1 p-2 rounded-md border focus:outline-none',
            error
              ? 'border-red-500 focus:ring-red-500 focus:border-red-500'
              : 'focus:ring-green-500 focus:border-green-500'
          )}
          ref={ref}
          {...props}
        />
        <p className={cx('text-sm', error ? 'text-red-500' : 'text-gray-400')}>{helperText}</p>
      </div>
    );
  }
);
