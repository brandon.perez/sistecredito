import * as React from 'react';
import cx from 'clsx';

export interface BannerProps {
  variant?: 'success' | 'error' | 'warning';
  children: JSX.Element | string;
}

export function Banner({ variant = 'warning', children }: BannerProps) {
  return (
    <div
      className={cx(
        'my-2 mx-6 shadow-sm rounded-md p-2 text-center font-medium leading-5',
        variant === 'success' && 'bg-green-200 text-green-800 ',
        variant === 'warning' && 'bg-yellow-200 text-yellow-800 ',
        variant === 'error' && 'bg-red-200 text-red-800 '
      )}
    >
      {children}
    </div>
  );
}
