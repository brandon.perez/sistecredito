/* eslint-disable react/button-has-type */
import * as React from 'react';
import cx from 'clsx';
import { ButtonProps } from '../types';

export function Button({ variant = 'outlined', disabled = false, children, ...props }: ButtonProps) {
  return (
    <div className="mx-2">
      <button
        className={cx(
          'w-full px-4 py-2 inline-flex justify-center rounded-md shadow-sm text-base font-medium border focus:outline-none focus:ring-2 focus:rign-offset-2',
          !disabled && variant === 'primary' && 'bg-green-500 text-white hover:bg-green-700 focus:ring-green-500',
          !disabled && variant === 'secondary' && 'bg-blue-500 text-white hover:bg-blue-700',
          !disabled && variant === 'outlined' && 'bg-white text-gray-700 border-gray-300 hover:bg-gray-100',
          disabled && 'bg-gray-200 text-gray-500 cursor-not-allowed'
        )}
        disabled={disabled}
        {...props}
      >
        {children}
      </button>
    </div>
  );
}
