import * as React from 'react';
import cx from 'clsx';
import { SelectProps } from '../types';

export const Select = React.forwardRef<HTMLSelectElement, SelectProps>(
  ({ id, label, placeholder, helperText, error, children, ...props }, ref) => {
    return (
      <div className="py-2">
        <label className={cx('block text-sm font-medium', error ? 'text-red-500' : 'text-gray-700')} htmlFor={id}>
          {label}
        </label>
        <select
          className={cx(
            'block w-full mt-1 p-2 rounded-md border focus:outline-none',
            error
              ? 'border-red-500 focus:ring-red-500 focus:border-red-500'
              : 'border-gray-300 focus:ring-green-500 focus:border-green-500 '
          )}
          defaultValue={-1}
          ref={ref}
          {...props}
        >
          <option value={-1}>{placeholder}</option>
          {children}
        </select>
        <p className="text-sm text-gray-400">{helperText}</p>
      </div>
    );
  }
);
