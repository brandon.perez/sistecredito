import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { PaymentButton } from '../src';

describe('it', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<PaymentButton callback="https://google.com/" subtotal={20000} credinetConfig={{ env: 'development', apiKey: 'api-key', vendorId: 'vendor-id' }} />, div); s
    ReactDOM.unmountComponentAtNode(div);
  });
});
