import 'react-app-polyfill/ie11';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ButtonCheckout } from '../.'

const App = () => {
  return (
    <div>
      Pagar
      <ButtonCheckout creditValue={20000} seller="LuegopaGo" invoice="1234" products="SOAT" responseUrl="localhost:9090/processing" renderInput={params => <button {...params}>Pagar con sistecredito sdsd</button> } />
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
